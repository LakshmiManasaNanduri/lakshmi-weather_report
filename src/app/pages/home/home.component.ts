import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment-timezone';
import { forkJoin } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Report } from 'src/app/interfaces/report.interface';
import { WeatherReportService } from 'src/app/services/weather.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  /**
   * statically defining the array of cities
   */
  public cities:any =[];
  public isLoading:boolean=true;
  selectedCity: any;
  public todayDate: string ='';
  /**
   * static Cities Data
   */
  private staticCitiesData = [
    'London','Prague','Paris','Amsterdam','Vienna'
  ];
  constructor(private router:Router,
    private weatherReportService: WeatherReportService,
    private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.generateArrayOfCitiesData();
  }
  /**
   * navigate to details
   */
  public navigateToDetails(selectedCity: any){
    this.selectedCity = selectedCity.name
    this.router.navigate(['city', selectedCity.name],{relativeTo:this.route})
   }
   /**
    * to check which city is selected
    * @param activeElem active div/city i.e selected
    * @returns 
    */
   public isActive(activeElem:string) {
    return this.selectedCity === activeElem;
  };

  /**
   * 
   */
  private generateArrayOfCitiesData():void{
    let queue: any[] = [];
     this.staticCitiesData.map((record:string)=>{
      queue.push(this.weatherReportService.getByCityName(record));
    });
    forkJoin(queue).pipe(finalize(()=>{
      this.isLoading =false;
     }))
     .subscribe((response:any)=>{
      if(response){
        response.forEach((cityData:Report)=>{
          this.cities.push(
            {
              name:cityData.name,
              imageSrc:`../../../assets/${cityData.name}_tile.jpeg`,
              time: this.getFormattedTime(cityData.dt, cityData.timezone),
              minTemprature:cityData.main.temp_min,
              maxTemprature:cityData.main.temp_max,
              sunrise:cityData.sys.sunrise,
              sunset:cityData.sys.sunset
            })
        })
      }
    },()=>{
      console.log('error');
      this.isLoading = false;
    });
  }
  /**
   * formating the date inorder to display
   * @param unix_timestamp timestamop from response json
   * @param timeZone time zone from response json
   * @returns string of date
   */
  private getFormattedTime(unix_timestamp: number,timeZone:number): string{
     let formattedTime ='';
    if(unix_timestamp){
    // getting time and date from unix_timestamp
    let dateTime = moment
    .unix(unix_timestamp)
    .tz('GMT')
    .format('YYYY-MM-DDTHH:mm:ssZ').split('T');
    // to add or delete according to time zone
    let timeAddOrSub = timeZone/3600;
    // hours 
    let hours = parseInt(dateTime[1].split(':')[0]) + timeAddOrSub;
    // mins
    let mins = parseInt(dateTime[1].split(':')[1]);
    // Will display time in 10:30:23 format
    formattedTime =  hours +':'+ mins;
  }
    return formattedTime+'' || '';
  }
}
