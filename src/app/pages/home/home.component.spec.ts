import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, fakeAsync, flush, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { forkJoin, Observable, of, throwError } from 'rxjs';
import { WeatherReportService } from 'src/app/services/weather.service';

import { HomeComponent } from './home.component';

const reportMockdata={
	"coord": {
		"lon": 14.4208,
		"lat": 50.088
	},
	"weather": [{
		"id": 800,
		"main": "Clear",
		"description": "clear sky",
		"icon": "01d"
	}],
	"base": "stations",
	"main": {
		"temp": 292.87,
		"feels_like": 292.67,
		"temp_min": 291.65,
		"temp_max": 294.73,
		"pressure": 1025,
		"humidity": 68
	},
	"visibility": 10000,
	"wind": {
		"speed": 0.45,
		"deg": 267,
		"gust": 1.34
	},
	"clouds": {
		"all": 0
	},
	"dt": 1631007026,
	"sys": {
		"type": 2,
		"id": 2010430,
		"country": "CZ",
		"sunrise": 1630988782,
		"sunset": 1631036081
	},
	"timezone": 7200,
	"id": 3067696,
	"name": "Prague",
	"cod": 200
}

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let router:Router;
  let route:ActivatedRoute;
  let weatherReportService:WeatherReportService;
 
  beforeEach(async () => {
    const serviceMock={
      getByCityName: jasmine.createSpy()
      .and
      .returnValue(of(reportMockdata))
    }
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [
        RouterTestingModule,
        HttpClientModule
      ],
      providers:[
        {
         provide: WeatherReportService,
         useValue:serviceMock
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    weatherReportService=TestBed.inject(WeatherReportService);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    fixture.detectChanges();
  });
  afterEach(() => {
    fixture.destroy();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should generate cities array of data',()=>{
    fixture.detectChanges();
    expect(component.cities.length).toEqual(5);
    expect(component.isLoading)
    .toEqual(false);
  });
  it('should handle when service throw error',()=>{
    (weatherReportService.getByCityName as jasmine.Spy).and.returnValue(throwError('error'));
    fixture.detectChanges();
    expect(component.isLoading)
    .toBe(false);
  });
  /**
   * should navigate to sub page details
   */
  it('should navigate to sub page details',()=>{
    (weatherReportService.getByCityName as jasmine.Spy).calls.reset();
    fixture.detectChanges();
    const routerSpy = spyOn(router,'navigate');
    // mocking the selected city
    const selectedCity = {
      name:'London'
    }
    // calling navigateToDetails
    component.navigateToDetails(selectedCity);
    expect(component.selectedCity).toEqual('London');
    // expecting router navigate 
    expect(routerSpy).toHaveBeenCalledWith(['city',selectedCity.name],{relativeTo:route});
  })
  it('should check which city is active',()=>{
    (weatherReportService.getByCityName as jasmine.Spy).calls.reset();
    const selectedCity = {
      name:'Paris'
    }
    component.isActive(selectedCity.name);
    // should be false as the seelectedCity would be
    expect(component.isActive(selectedCity.name)).toBeFalse();
    component.selectedCity = 'Paris';
    component.isActive(selectedCity.name);
    // should be false as the seelectedCity would be
    expect(component.isActive(selectedCity.name)).toBeTruthy();
  })
});
