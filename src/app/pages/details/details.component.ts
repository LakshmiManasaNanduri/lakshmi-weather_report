import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError} from '@angular/router';
import { WeatherForecastService } from 'src/app/services/weather-forecast.service';
import * as  Highcharts from 'highcharts';
import * as moment from 'moment';
/**
 * Component for weather forecast details
 */
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  /**
   * current city selected
   */
  public  selectedCity = '';
  /**
   * one of the input for the highcharts
   */
  public highcharts = Highcharts;
  public isLoading = true;
  /**
   * chart Data to show the forecast of temprature ans sealevel
   */
  public chartOptions: Highcharts.Options ={};
  /**
   * initialises the component
   * @param weatherForecastService WeatherForecastService
   * @param router Router
   * @param route ActivatedRoute
   */
  constructor(private weatherForecastService: WeatherForecastService,
    private router:Router) {
      // detecting the url change event using router.events
      this.router.events.subscribe((event: Event) => {
        if (event instanceof NavigationStart) {
            // Show progress spinner or progress bar
            console.log('Route change detected');
        }

        if (event instanceof NavigationEnd) {
            // Hide progress spinner or progress bar
            this.selectedCity = event.url.split('/')[3];          
            console.log(event);
            this.getWeatherForecast();
          
        }

        if (event instanceof NavigationError) {
             // Hide progress spinner or progress bar if required

            // Present error to user
            console.log(event.error);
        }
    });
    
    
   }

  ngOnInit(): void {
  }
  /**
   * get the weather forecast for next 5 days for the selected city
   */
  public getWeatherForecast(): void{
    this.chartOptions={};
    this.weatherForecastService.getForecast(this.selectedCity).subscribe((forecastResponse:any)=>{
      if(forecastResponse.list){
        // filtering the data which are having time 9:00
        let filterData = forecastResponse.list.filter((data: { dt_txt: any; })=> data.dt_txt.split(' ')[1].split(':')[0]==9)
        // transforming the response to high chart compatible object 
        this.transformToChart(filterData);
        this.isLoading = false;
      }
    },()=>{
      this.isLoading = false;
      console.log('error');
    });
  }
  /**
   * transforming the response to high chart compatible object
   * @param filterData data has been filtered depending on condition
   */
  public transformToChart(filterData: any):void {
    
    let xaxisArray: string[] =[];
    let tempratureData:any=[];
    let seaLevelData:any=[];
    // iterating thrgh loop to assign varibles appropriate
    filterData.forEach((record:any)=>{
      xaxisArray.push(moment(record.dt_txt, "YYYY/MM/DD").format("DD MMM"));
      tempratureData.push(record.main.temp);
      seaLevelData.push(record.main.sea_level);
    })
    
    this.chartOptions={
      title: {
        text: "Weather Forecast"
      },
      xAxis: {
        categories: xaxisArray
      },
      yAxis: {
        title: {
          text: "Temperature",
        }
      },
      series: [{
        name:'Temperature',
        data: tempratureData,
        type: 'line'
      },
      {
        name:'Sea Level',
        data: seaLevelData,
        type: 'line'
      }]
    };
  }
}
