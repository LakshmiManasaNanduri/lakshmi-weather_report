import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, fakeAsync, flush, TestBed } from '@angular/core/testing';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, ReplaySubject, throwError } from 'rxjs';
import { WeatherForecastService } from 'src/app/services/weather-forecast.service';

import { DetailsComponent } from './details.component';
const mockResponse ={
	"cod": "200",
	"message": 0,
	"cnt": 40,
	"list": [{
		"dt": 1631113200,
		"main": {
			"temp": 297.94,
			"feels_like": 297.49,
			"temp_min": 297.72,
			"temp_max": 297.94,
			"pressure": 1019,
			"sea_level": 1019,
			"grnd_level": 994,
			"humidity": 39,
			"temp_kf": 0.22
		},
		"dt_txt": "2021-09-08 15:00:00"
	}, {
		"dt": 1631124000,
		"main": {
			"temp": 295.79,
			"feels_like": 295.23,
			"temp_min": 291.48,
			"temp_max": 295.79,
			"pressure": 1018,
			"sea_level": 1018,
			"grnd_level": 993,
			"humidity": 43,
			"temp_kf": 4.31
		},
		"dt_txt": "2021-09-08 18:00:00"
	}, {
		"dt": 1631134800,
		"main": {
			"temp": 291.88,
			"feels_like": 291.06,
			"temp_min": 288.85,
			"temp_max": 291.88,
			"pressure": 1018,
			"sea_level": 1018,
			"grnd_level": 993,
			"humidity": 48,
			"temp_kf": 3.03
		},
		"weather": [{
			"id": 800,
			"main": "Clear",
			"description": "clear sky",
			"icon": "01n"
		}],
		"clouds": {
			"all": 5
		},
		"wind": {
			"speed": 2.81,
			"deg": 127,
			"gust": 7.54
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-08 21:00:00"
	}, {
		"dt": 1631145600,
		"main": {
			"temp": 286.54,
			"feels_like": 285.4,
			"temp_min": 286.54,
			"temp_max": 286.54,
			"pressure": 1016,
			"sea_level": 1016,
			"grnd_level": 992,
			"humidity": 56,
			"temp_kf": 0
		},
		"weather": [{
			"id": 800,
			"main": "Clear",
			"description": "clear sky",
			"icon": "01n"
		}],
		"clouds": {
			"all": 5
		},
		"wind": {
			"speed": 1.85,
			"deg": 107,
			"gust": 2.62
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-09 00:00:00"
	}, {
		"dt": 1631156400,
		"main": {
			"temp": 285.21,
			"feels_like": 284.04,
			"temp_min": 285.21,
			"temp_max": 285.21,
			"pressure": 1016,
			"sea_level": 1016,
			"grnd_level": 992,
			"humidity": 60,
			"temp_kf": 0
		},
		"weather": [{
			"id": 800,
			"main": "Clear",
			"description": "clear sky",
			"icon": "01n"
		}],
		"clouds": {
			"all": 6
		},
		"wind": {
			"speed": 1.47,
			"deg": 140,
			"gust": 1.81
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-09 03:00:00"
	}, {
		"dt": 1631167200,
		"main": {
			"temp": 286.83,
			"feels_like": 285.77,
			"temp_min": 286.83,
			"temp_max": 286.83,
			"pressure": 1016,
			"sea_level": 1016,
			"grnd_level": 992,
			"humidity": 58,
			"temp_kf": 0
		},
		"weather": [{
			"id": 800,
			"main": "Clear",
			"description": "clear sky",
			"icon": "01d"
		}],
		"clouds": {
			"all": 6
		},
		"wind": {
			"speed": 1.37,
			"deg": 157,
			"gust": 2.45
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-09 06:00:00"
	}, {
		"dt": 1631178000,
		"main": {
			"temp": 293.96,
			"feels_like": 293.17,
			"temp_min": 293.96,
			"temp_max": 293.96,
			"pressure": 1016,
			"sea_level": 1016,
			"grnd_level": 992,
			"humidity": 41,
			"temp_kf": 0
		},
		"weather": [{
			"id": 800,
			"main": "Clear",
			"description": "clear sky",
			"icon": "01d"
		}],
		"clouds": {
			"all": 3
		},
		"wind": {
			"speed": 2.51,
			"deg": 169,
			"gust": 3.73
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-09 09:00:00"
	}, {
		"dt": 1631188800,
		"main": {
			"temp": 298.68,
			"feels_like": 298.2,
			"temp_min": 298.68,
			"temp_max": 298.68,
			"pressure": 1014,
			"sea_level": 1014,
			"grnd_level": 991,
			"humidity": 35,
			"temp_kf": 0
		},
		"weather": [{
			"id": 800,
			"main": "Clear",
			"description": "clear sky",
			"icon": "01d"
		}],
		"clouds": {
			"all": 1
		},
		"wind": {
			"speed": 3.35,
			"deg": 176,
			"gust": 4.21
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-09 12:00:00"
	}, {
		"dt": 1631199600,
		"main": {
			"temp": 299.65,
			"feels_like": 299.65,
			"temp_min": 299.65,
			"temp_max": 299.65,
			"pressure": 1013,
			"sea_level": 1013,
			"grnd_level": 990,
			"humidity": 37,
			"temp_kf": 0
		},
		"weather": [{
			"id": 800,
			"main": "Clear",
			"description": "clear sky",
			"icon": "01d"
		}],
		"clouds": {
			"all": 0
		},
		"wind": {
			"speed": 3.07,
			"deg": 174,
			"gust": 3.99
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-09 15:00:00"
	}, {
		"dt": 1631210400,
		"main": {
			"temp": 293.65,
			"feels_like": 293.14,
			"temp_min": 293.65,
			"temp_max": 293.65,
			"pressure": 1013,
			"sea_level": 1013,
			"grnd_level": 990,
			"humidity": 53,
			"temp_kf": 0
		},
		"weather": [{
			"id": 800,
			"main": "Clear",
			"description": "clear sky",
			"icon": "01n"
		}],
		"clouds": {
			"all": 1
		},
		"wind": {
			"speed": 2.42,
			"deg": 155,
			"gust": 3.72
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-09 18:00:00"
	}, {
		"dt": 1631221200,
		"main": {
			"temp": 291.03,
			"feels_like": 290.36,
			"temp_min": 291.03,
			"temp_max": 291.03,
			"pressure": 1015,
			"sea_level": 1015,
			"grnd_level": 991,
			"humidity": 57,
			"temp_kf": 0
		},
		"weather": [{
			"id": 800,
			"main": "Clear",
			"description": "clear sky",
			"icon": "01n"
		}],
		"clouds": {
			"all": 1
		},
		"wind": {
			"speed": 1.84,
			"deg": 172,
			"gust": 2.61
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-09 21:00:00"
	}, {
		"dt": 1631232000,
		"main": {
			"temp": 289.43,
			"feels_like": 288.84,
			"temp_min": 289.43,
			"temp_max": 289.43,
			"pressure": 1015,
			"sea_level": 1015,
			"grnd_level": 991,
			"humidity": 66,
			"temp_kf": 0
		},
		"weather": [{
			"id": 800,
			"main": "Clear",
			"description": "clear sky",
			"icon": "01n"
		}],
		"clouds": {
			"all": 2
		},
		"wind": {
			"speed": 1.2,
			"deg": 166,
			"gust": 1.6
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-10 00:00:00"
	}, {
		"dt": 1631242800,
		"main": {
			"temp": 288.56,
			"feels_like": 288.19,
			"temp_min": 288.56,
			"temp_max": 288.56,
			"pressure": 1015,
			"sea_level": 1015,
			"grnd_level": 991,
			"humidity": 78,
			"temp_kf": 0
		},
		"weather": [{
			"id": 800,
			"main": "Clear",
			"description": "clear sky",
			"icon": "01n"
		}],
		"clouds": {
			"all": 4
		},
		"wind": {
			"speed": 1.46,
			"deg": 179,
			"gust": 1.86
		},
		"visibility": 10000,
		"pop": 0.17,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-10 03:00:00"
	}, {
		"dt": 1631253600,
		"main": {
			"temp": 290.43,
			"feels_like": 290.25,
			"temp_min": 290.43,
			"temp_max": 290.43,
			"pressure": 1016,
			"sea_level": 1016,
			"grnd_level": 992,
			"humidity": 78,
			"temp_kf": 0
		},
		"weather": [{
			"id": 801,
			"main": "Clouds",
			"description": "few clouds",
			"icon": "02d"
		}],
		"clouds": {
			"all": 12
		},
		"wind": {
			"speed": 1.05,
			"deg": 179,
			"gust": 1.8
		},
		"visibility": 10000,
		"pop": 0.14,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-10 06:00:00"
	}, {
		"dt": 1631264400,
		"main": {
			"temp": 295.55,
			"feels_like": 295.46,
			"temp_min": 295.55,
			"temp_max": 295.55,
			"pressure": 1016,
			"sea_level": 1016,
			"grnd_level": 992,
			"humidity": 62,
			"temp_kf": 0
		},
		"weather": [{
			"id": 803,
			"main": "Clouds",
			"description": "broken clouds",
			"icon": "04d"
		}],
		"clouds": {
			"all": 64
		},
		"wind": {
			"speed": 1.73,
			"deg": 191,
			"gust": 2.23
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-10 09:00:00"
	}, {
		"dt": 1631275200,
		"main": {
			"temp": 298.65,
			"feels_like": 298.59,
			"temp_min": 298.65,
			"temp_max": 298.65,
			"pressure": 1015,
			"sea_level": 1015,
			"grnd_level": 992,
			"humidity": 51,
			"temp_kf": 0
		},
		"weather": [{
			"id": 803,
			"main": "Clouds",
			"description": "broken clouds",
			"icon": "04d"
		}],
		"clouds": {
			"all": 73
		},
		"wind": {
			"speed": 1.99,
			"deg": 180,
			"gust": 2.42
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-10 12:00:00"
	}, {
		"dt": 1631286000,
		"main": {
			"temp": 299.64,
			"feels_like": 299.64,
			"temp_min": 299.64,
			"temp_max": 299.64,
			"pressure": 1014,
			"sea_level": 1014,
			"grnd_level": 991,
			"humidity": 47,
			"temp_kf": 0
		},
		"weather": [{
			"id": 802,
			"main": "Clouds",
			"description": "scattered clouds",
			"icon": "03d"
		}],
		"clouds": {
			"all": 43
		},
		"wind": {
			"speed": 2.34,
			"deg": 151,
			"gust": 2.53
		},
		"visibility": 10000,
		"pop": 0.03,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-10 15:00:00"
	}, {
		"dt": 1631296800,
		"main": {
			"temp": 293.53,
			"feels_like": 293.56,
			"temp_min": 293.53,
			"temp_max": 293.53,
			"pressure": 1015,
			"sea_level": 1015,
			"grnd_level": 991,
			"humidity": 74,
			"temp_kf": 0
		},
		"weather": [{
			"id": 500,
			"main": "Rain",
			"description": "light rain",
			"icon": "10n"
		}],
		"clouds": {
			"all": 44
		},
		"wind": {
			"speed": 2.1,
			"deg": 156,
			"gust": 2.87
		},
		"visibility": 10000,
		"pop": 0.62,
		"rain": {
			"3h": 2.17
		},
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-10 18:00:00"
	}, {
		"dt": 1631307600,
		"main": {
			"temp": 291.21,
			"feels_like": 291.26,
			"temp_min": 291.21,
			"temp_max": 291.21,
			"pressure": 1016,
			"sea_level": 1016,
			"grnd_level": 992,
			"humidity": 84,
			"temp_kf": 0
		},
		"weather": [{
			"id": 501,
			"main": "Rain",
			"description": "moderate rain",
			"icon": "10n"
		}],
		"clouds": {
			"all": 68
		},
		"wind": {
			"speed": 2.01,
			"deg": 181,
			"gust": 3.19
		},
		"visibility": 9792,
		"pop": 0.86,
		"rain": {
			"3h": 4.05
		},
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-10 21:00:00"
	}, {
		"dt": 1631318400,
		"main": {
			"temp": 290.25,
			"feels_like": 290.29,
			"temp_min": 290.25,
			"temp_max": 290.25,
			"pressure": 1017,
			"sea_level": 1017,
			"grnd_level": 993,
			"humidity": 87,
			"temp_kf": 0
		},
		"weather": [{
			"id": 501,
			"main": "Rain",
			"description": "moderate rain",
			"icon": "10n"
		}],
		"clouds": {
			"all": 83
		},
		"wind": {
			"speed": 1.73,
			"deg": 191,
			"gust": 2.67
		},
		"visibility": 10000,
		"pop": 0.86,
		"rain": {
			"3h": 5.09
		},
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-11 00:00:00"
	}, {
		"dt": 1631329200,
		"main": {
			"temp": 289.85,
			"feels_like": 289.93,
			"temp_min": 289.85,
			"temp_max": 289.85,
			"pressure": 1017,
			"sea_level": 1017,
			"grnd_level": 993,
			"humidity": 90,
			"temp_kf": 0
		},
		"weather": [{
			"id": 500,
			"main": "Rain",
			"description": "light rain",
			"icon": "10n"
		}],
		"clouds": {
			"all": 100
		},
		"wind": {
			"speed": 0.81,
			"deg": 241,
			"gust": 1.35
		},
		"visibility": 10000,
		"pop": 0.84,
		"rain": {
			"3h": 1.5
		},
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-11 03:00:00"
	}, {
		"dt": 1631340000,
		"main": {
			"temp": 290.17,
			"feels_like": 290.28,
			"temp_min": 290.17,
			"temp_max": 290.17,
			"pressure": 1018,
			"sea_level": 1018,
			"grnd_level": 994,
			"humidity": 90,
			"temp_kf": 0
		},
		"weather": [{
			"id": 500,
			"main": "Rain",
			"description": "light rain",
			"icon": "10d"
		}],
		"clouds": {
			"all": 99
		},
		"wind": {
			"speed": 1.09,
			"deg": 259,
			"gust": 1.98
		},
		"visibility": 10000,
		"pop": 0.82,
		"rain": {
			"3h": 0.78
		},
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-11 06:00:00"
	}, {
		"dt": 1631350800,
		"main": {
			"temp": 293.56,
			"feels_like": 293.61,
			"temp_min": 293.56,
			"temp_max": 293.56,
			"pressure": 1019,
			"sea_level": 1019,
			"grnd_level": 995,
			"humidity": 75,
			"temp_kf": 0
		},
		"weather": [{
			"id": 500,
			"main": "Rain",
			"description": "light rain",
			"icon": "10d"
		}],
		"clouds": {
			"all": 100
		},
		"wind": {
			"speed": 1.79,
			"deg": 282,
			"gust": 2.74
		},
		"visibility": 10000,
		"pop": 0.4,
		"rain": {
			"3h": 0.52
		},
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-11 09:00:00"
	}, {
		"dt": 1631361600,
		"main": {
			"temp": 294.25,
			"feels_like": 294.27,
			"temp_min": 294.25,
			"temp_max": 294.25,
			"pressure": 1018,
			"sea_level": 1018,
			"grnd_level": 994,
			"humidity": 71,
			"temp_kf": 0
		},
		"weather": [{
			"id": 804,
			"main": "Clouds",
			"description": "overcast clouds",
			"icon": "04d"
		}],
		"clouds": {
			"all": 100
		},
		"wind": {
			"speed": 2.95,
			"deg": 267,
			"gust": 3.72
		},
		"visibility": 10000,
		"pop": 0.12,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-11 12:00:00"
	}, {
		"dt": 1631372400,
		"main": {
			"temp": 293.33,
			"feels_like": 293.39,
			"temp_min": 293.33,
			"temp_max": 293.33,
			"pressure": 1018,
			"sea_level": 1018,
			"grnd_level": 994,
			"humidity": 76,
			"temp_kf": 0
		},
		"weather": [{
			"id": 500,
			"main": "Rain",
			"description": "light rain",
			"icon": "10d"
		}],
		"clouds": {
			"all": 100
		},
		"wind": {
			"speed": 1.55,
			"deg": 200,
			"gust": 2.86
		},
		"visibility": 10000,
		"pop": 0.34,
		"rain": {
			"3h": 0.14
		},
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-11 15:00:00"
	}, {
		"dt": 1631383200,
		"main": {
			"temp": 291.16,
			"feels_like": 291.16,
			"temp_min": 291.16,
			"temp_max": 291.16,
			"pressure": 1018,
			"sea_level": 1018,
			"grnd_level": 994,
			"humidity": 82,
			"temp_kf": 0
		},
		"weather": [{
			"id": 804,
			"main": "Clouds",
			"description": "overcast clouds",
			"icon": "04n"
		}],
		"clouds": {
			"all": 99
		},
		"wind": {
			"speed": 2.86,
			"deg": 245,
			"gust": 7.38
		},
		"visibility": 10000,
		"pop": 0.39,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-11 18:00:00"
	}, {
		"dt": 1631394000,
		"main": {
			"temp": 289.45,
			"feels_like": 289.41,
			"temp_min": 289.45,
			"temp_max": 289.45,
			"pressure": 1019,
			"sea_level": 1019,
			"grnd_level": 994,
			"humidity": 87,
			"temp_kf": 0
		},
		"weather": [{
			"id": 801,
			"main": "Clouds",
			"description": "few clouds",
			"icon": "02n"
		}],
		"clouds": {
			"all": 23
		},
		"wind": {
			"speed": 2.47,
			"deg": 238,
			"gust": 4.43
		},
		"visibility": 10000,
		"pop": 0.19,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-11 21:00:00"
	}, {
		"dt": 1631404800,
		"main": {
			"temp": 288.6,
			"feels_like": 288.52,
			"temp_min": 288.6,
			"temp_max": 288.6,
			"pressure": 1018,
			"sea_level": 1018,
			"grnd_level": 994,
			"humidity": 89,
			"temp_kf": 0
		},
		"weather": [{
			"id": 801,
			"main": "Clouds",
			"description": "few clouds",
			"icon": "02n"
		}],
		"clouds": {
			"all": 12
		},
		"wind": {
			"speed": 2.09,
			"deg": 237,
			"gust": 2.62
		},
		"visibility": 10000,
		"pop": 0.16,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-12 00:00:00"
	}, {
		"dt": 1631415600,
		"main": {
			"temp": 287.74,
			"feels_like": 287.6,
			"temp_min": 287.74,
			"temp_max": 287.74,
			"pressure": 1018,
			"sea_level": 1018,
			"grnd_level": 994,
			"humidity": 90,
			"temp_kf": 0
		},
		"weather": [{
			"id": 801,
			"main": "Clouds",
			"description": "few clouds",
			"icon": "02n"
		}],
		"clouds": {
			"all": 15
		},
		"wind": {
			"speed": 1.82,
			"deg": 245,
			"gust": 2.37
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-12 03:00:00"
	}, {
		"dt": 1631426400,
		"main": {
			"temp": 288.87,
			"feels_like": 288.69,
			"temp_min": 288.87,
			"temp_max": 288.87,
			"pressure": 1018,
			"sea_level": 1018,
			"grnd_level": 994,
			"humidity": 84,
			"temp_kf": 0
		},
		"weather": [{
			"id": 802,
			"main": "Clouds",
			"description": "scattered clouds",
			"icon": "03d"
		}],
		"clouds": {
			"all": 25
		},
		"wind": {
			"speed": 1.56,
			"deg": 235,
			"gust": 2.74
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-12 06:00:00"
	}, {
		"dt": 1631437200,
		"main": {
			"temp": 294.43,
			"feels_like": 294.13,
			"temp_min": 294.43,
			"temp_max": 294.43,
			"pressure": 1018,
			"sea_level": 1018,
			"grnd_level": 994,
			"humidity": 58,
			"temp_kf": 0
		},
		"weather": [{
			"id": 803,
			"main": "Clouds",
			"description": "broken clouds",
			"icon": "04d"
		}],
		"clouds": {
			"all": 57
		},
		"wind": {
			"speed": 2.16,
			"deg": 256,
			"gust": 2.92
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-12 09:00:00"
	}, {
		"dt": 1631448000,
		"main": {
			"temp": 297.35,
			"feels_like": 297,
			"temp_min": 297.35,
			"temp_max": 297.35,
			"pressure": 1016,
			"sea_level": 1016,
			"grnd_level": 993,
			"humidity": 45,
			"temp_kf": 0
		},
		"weather": [{
			"id": 803,
			"main": "Clouds",
			"description": "broken clouds",
			"icon": "04d"
		}],
		"clouds": {
			"all": 64
		},
		"wind": {
			"speed": 2.56,
			"deg": 252,
			"gust": 3.73
		},
		"visibility": 10000,
		"pop": 0.02,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-12 12:00:00"
	}, {
		"dt": 1631458800,
		"main": {
			"temp": 296.84,
			"feels_like": 296.52,
			"temp_min": 296.84,
			"temp_max": 296.84,
			"pressure": 1015,
			"sea_level": 1015,
			"grnd_level": 992,
			"humidity": 48,
			"temp_kf": 0
		},
		"weather": [{
			"id": 803,
			"main": "Clouds",
			"description": "broken clouds",
			"icon": "04d"
		}],
		"clouds": {
			"all": 79
		},
		"wind": {
			"speed": 3.08,
			"deg": 262,
			"gust": 3.54
		},
		"visibility": 10000,
		"pop": 0.02,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-12 15:00:00"
	}, {
		"dt": 1631469600,
		"main": {
			"temp": 292.96,
			"feels_like": 292.67,
			"temp_min": 292.96,
			"temp_max": 292.96,
			"pressure": 1015,
			"sea_level": 1015,
			"grnd_level": 992,
			"humidity": 64,
			"temp_kf": 0
		},
		"weather": [{
			"id": 804,
			"main": "Clouds",
			"description": "overcast clouds",
			"icon": "04n"
		}],
		"clouds": {
			"all": 88
		},
		"wind": {
			"speed": 1.07,
			"deg": 270,
			"gust": 1.78
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-12 18:00:00"
	}, {
		"dt": 1631480400,
		"main": {
			"temp": 290.93,
			"feels_like": 290.64,
			"temp_min": 290.93,
			"temp_max": 290.93,
			"pressure": 1016,
			"sea_level": 1016,
			"grnd_level": 992,
			"humidity": 72,
			"temp_kf": 0
		},
		"weather": [{
			"id": 804,
			"main": "Clouds",
			"description": "overcast clouds",
			"icon": "04n"
		}],
		"clouds": {
			"all": 100
		},
		"wind": {
			"speed": 1.16,
			"deg": 255,
			"gust": 1.49
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2021-09-12 21:00:00"
	}, {
		"dt": 1631491200,
		"main": {
			"temp": 290.39,
			"feels_like": 290.1,
			"temp_min": 290.39,
			"temp_max": 290.39,
			"pressure": 1015,
			"sea_level": 1015,
			"grnd_level": 991,
			"humidity": 74,
			"temp_kf": 0
		},
		"weather": [{
			"id": 804,
			"main": "Clouds",
			"description": "overcast clouds",
			"icon": "04d"
		}],
		"clouds": {
			"all": 100
		},
		"wind": {
			"speed": 0.78,
			"deg": 275,
			"gust": 0.95
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-13 00:00:00"
	}, {
		"dt": 1631502000,
		"main": {
			"temp": 289.55,
			"feels_like": 289.26,
			"temp_min": 289.55,
			"temp_max": 289.55,
			"pressure": 1015,
			"sea_level": 1015,
			"grnd_level": 991,
			"humidity": 77,
			"temp_kf": 0
		},
		"weather": [{
			"id": 804,
			"main": "Clouds",
			"description": "overcast clouds",
			"icon": "04d"
		}],
		"clouds": {
			"all": 100
		},
		"wind": {
			"speed": 0.59,
			"deg": 335,
			"gust": 0.64
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-13 03:00:00"
	}, {
		"dt": 1631512800,
		"main": {
			"temp": 290.13,
			"feels_like": 289.87,
			"temp_min": 290.13,
			"temp_max": 290.13,
			"pressure": 1015,
			"sea_level": 1015,
			"grnd_level": 991,
			"humidity": 76,
			"temp_kf": 0
		},
		"weather": [{
			"id": 804,
			"main": "Clouds",
			"description": "overcast clouds",
			"icon": "04d"
		}],
		"clouds": {
			"all": 100
		},
		"wind": {
			"speed": 0.55,
			"deg": 36,
			"gust": 0.71
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-13 06:00:00"
	}, {
		"dt": 1631523600,
		"main": {
			"temp": 293.97,
			"feels_like": 293.7,
			"temp_min": 293.97,
			"temp_max": 293.97,
			"pressure": 1015,
			"sea_level": 1015,
			"grnd_level": 991,
			"humidity": 61,
			"temp_kf": 0
		},
		"weather": [{
			"id": 804,
			"main": "Clouds",
			"description": "overcast clouds",
			"icon": "04d"
		}],
		"clouds": {
			"all": 100
		},
		"wind": {
			"speed": 1.98,
			"deg": 75,
			"gust": 2.49
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-13 09:00:00"
	}, {
		"dt": 1631534400,
		"main": {
			"temp": 297.36,
			"feels_like": 297.09,
			"temp_min": 297.36,
			"temp_max": 297.36,
			"pressure": 1013,
			"sea_level": 1013,
			"grnd_level": 990,
			"humidity": 48,
			"temp_kf": 0
		},
		"weather": [{
			"id": 804,
			"main": "Clouds",
			"description": "overcast clouds",
			"icon": "04d"
		}],
		"clouds": {
			"all": 100
		},
		"wind": {
			"speed": 2.94,
			"deg": 96,
			"gust": 4.05
		},
		"visibility": 10000,
		"pop": 0,
		"sys": {
			"pod": "d"
		},
		"dt_txt": "2021-09-13 12:00:00"
	}],
	"city": {
		"id": 3067696,
		"name": "Prague",
		"coord": {
			"lat": 50.088,
			"lon": 14.4208
		},
		"country": "CZ",
		"population": 1165581,
		"timezone": 7200,
		"sunrise": 1631075271,
		"sunset": 1631122350
	}
};
describe('Details Component', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;
  let weatherForecastService: WeatherForecastService;
  let methodspy:jasmine.Spy;
  const evntSub = new ReplaySubject<RouterEvent>(1);
  const routerMock ={
    navgate:jasmine.createSpy('navigate'),
    events:evntSub,
    url:'/home/city/London'
  }
  
  beforeEach(async () => {
    const serviceMock ={
      getForecast: jasmine.createSpy()
	  .and
	  .returnValue(of(mockResponse))
    }
    await TestBed.configureTestingModule({
      declarations: [ DetailsComponent ],

      imports: [
        RouterTestingModule,
        HttpClientModule],
      providers:[
        {
          provide:Router,
          useValue:routerMock
        },
        {
          provide:WeatherForecastService,
        	useValue:serviceMock
      }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    weatherForecastService = TestBed.inject(WeatherForecastService);
  });

  afterEach(() => {
    fixture.destroy();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call getWeatherForecast on url change event',fakeAsync(()=>{
    methodspy = spyOn(component,'getWeatherForecast');
    evntSub.next(new NavigationEnd(1,'London','redirectUrl'));
    expect(methodspy).toHaveBeenCalled();
    flush();
  }));
  it('should get data for the getWeatherForecast',()=>{
    (weatherForecastService.getForecast as jasmine.Spy).calls.reset();
    (weatherForecastService.getForecast as jasmine.Spy).and.returnValue(of(mockResponse));
    fixture.detectChanges();
    component.transformToChart(mockResponse.list.filter((data: { dt_txt: any; })=> data.dt_txt.split(' ')[1].split(':')[0]==9))
    expect(component.chartOptions.xAxis).toEqual({ categories: [ '09 Sep', '10 Sep', '11 Sep', '12 Sep', '13 Sep' ] });
  });
  it('should throw Error',()=>{
    (weatherForecastService.getForecast as jasmine.Spy).calls.reset();
    (weatherForecastService.getForecast as jasmine.Spy).and.returnValue(throwError('error'));
    fixture.detectChanges();
    expect(component.chartOptions).toEqual({});
  });
});
