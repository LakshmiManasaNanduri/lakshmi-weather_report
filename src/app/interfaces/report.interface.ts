import { Cordiantes } from "./cordinates.interface";

export interface Report{
	/**
	 * Cordiantes
	 */
    coord: Cordiantes;
	/**
	 * Weather
	 */
	weather?: [{
		id: 721,
		main: "Haze",
		description: "haze",
		icon: "50d"
	}],
	/**
	 * base
	 */
	base?: string,
	/**
	 * main details
	 */
	main: {
		temp: number,
		feels_like?: number,
		temp_min: number,
		temp_max: number,
		pressure?: number,
		humidity?: number
	},
	/**
	 * visibility
	 */
	visibility?: number,
	/**
	 * wind details
	 */
	wind?: {
		speed: number,
		deg: number
	},
	/**
	 * clouds
	 */
	clouds?: {
		all: number
	},
	/**
	 * time and date
	 */
	dt: number,
	/**
	 * sun raise and sunset details
	 */
	sys: {
		type?: 2,
		id?: 2006068,
		country?: "GB",
		sunrise: 1631165166,
		sunset: 1631212193
	},
	/**
	 * time zone
	 */
	timezone: number,
	/**
	 * id
	 */
	id?: number,
	/**
	 * name of the city
	 */
	name: string,
	/**
	 * cod
	 */
	cod?: number
}