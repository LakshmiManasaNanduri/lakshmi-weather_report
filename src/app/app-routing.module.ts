import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsComponent } from './pages/details/details.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  // landing page
  {
    path:'home',
    component:HomeComponent,
    children:[
      // sub page when clicked on city
        {
          path:'city/:name',
          component:DetailsComponent
        },
    ]
  },
  // Default redirection if nothing matches
  {
    path:'**',
    redirectTo:'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
