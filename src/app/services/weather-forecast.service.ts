import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
/**
 * appid
 */
const appid ='3d8b309701a13f65b660fa2c64cdc517';
@Injectable({
  providedIn:'root'  
})
export class WeatherForecastService{
    /**
     * openweathermap URL
     */
    public readonly url = 'http://api.openweathermap.org/data/2.5/forecast';
    /**
     * httpClient
     */
     private httpClient: HttpClient
    constructor( httpClient: HttpClient){
        this.httpClient=httpClient;
    }
    /**
     * getting city name
     * @param name city name
     */
    public getForecast(name:string): Observable<any>{
      let urll = this.url  +`?q=${name}&appid=${appid}`
      return this.httpClient.get<any>(urll);
    }
}