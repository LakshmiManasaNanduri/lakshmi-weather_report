import { WeatherReportService } from "./weather.service"
import { HttpClientTestingModule,HttpTestingController } from "@angular/common/http/testing"
import { TestBed } from "@angular/core/testing";

const appid = '3d8b309701a13f65b660fa2c64cdc517';
const name ='London'
const url = `http://api.openweathermap.org/data/2.5/weather?q=${name}&appid=${appid}`
describe('Weather Report service',()=>{
    let service : WeatherReportService
    let httpTestingController: HttpTestingController;

    beforeEach(()=>{
        TestBed.configureTestingModule({
            imports:[
                HttpClientTestingModule
            ],
            providers:[WeatherReportService]
        });
        // inject the http service and test controller for each test
        httpTestingController = TestBed.inject(HttpTestingController);

        // inject the service
        service = TestBed.inject(WeatherReportService);
    });
    afterEach(()=>{
        httpTestingController.verify();
    })
    it('should be created',()=>{
        expect(service).toBeTruthy();
    });
    it('should get the weather report of particular city that is selected',(done:DoneFn)=>{
        service.getByCityName('London').subscribe((response:any)=>{
            expect(response).toEqual({});
            done();
        });
        const req = httpTestingController.expectOne(url);
        expect(req.request.method)
        .toBe('GET');
        req.flush({});
    });
})