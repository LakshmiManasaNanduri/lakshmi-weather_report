import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { Report } from "../interfaces/report.interface";
/**
 * appid
 */
const appid ='3d8b309701a13f65b660fa2c64cdc517';
@Injectable({
  providedIn:'root'  
})
export class WeatherReportService{
    /**
     * openweathermap URL
     */
    public readonly url = 'http://api.openweathermap.org/data/2.5/weather';
    /**
     * httpClient
     */
     private httpClient: HttpClient
    constructor( httpClient: HttpClient){
        this.httpClient=httpClient;
    }
    /**
     * getting city name
     * @param name city name
     */
    public getByCityName(name:string): Observable<Report>{
        let urll=this.url+`?q=${name}&appid=${appid}`
        console.log(urll);
      return this.httpClient.get<any>(urll);
    }
}